/*
  Hit CTRL/CMD + D to run the code, view console for results
*/
import { sp } from "@pnp/sp/presets/all";

(async () => {
  console.log(await (window as any).moduleLoaderPromise)
  console.log((window as any))
})();

sp.setup({
  sp: {
    headers: {
      Accept: "application/json;odata=verbose",
    },
    baseUrl: ""
  },
});

const list = "list1"
const viewName = "test1"

sp.web.lists.getByTitle("list1").views
  .getByTitle("test1")
  .select("Id")
  .get()
  .then((v) => {

    const viewId = v.Id;
    // view
    //  filter ok
    //  sort ok

    // table
    //  filter ok
    //  sort

    const param = new Map<string, string>();
    // 条件1
    param.set("FilterField1", "Bool");
    param.set("FilterValue1", "0");
    param.set("FilterType1", "Boolean");
    // 条件2
    //param.set("FilterField2", "LinkTitle");
    //param.set("FilterValue2", "test-01");
    //param.set("FilterType2", "Computed");
    param.set("SortField1", "Num")
    param.set("SortDir1", "Desc")
    const reslts = [];
    let cnt = 0;
    async function relad(prevRes?) {
      if (cnt === 10) {
        //return;
      }
      let next: string | undefined = undefined;
      if (prevRes) {
        // 先頭の?を除去(あると同じ検索を繰り返す)
        next = prevRes.NextHref.split("?")[1];
      }
      //a59b1efc-a852-4ea5-b302-b4bca4b80aa1
      const res = await sp.web.lists
        .getByTitle(list)
        .renderListDataAsStream({
          Paging: next,
          //RenderOptions: 135175,
          AddRequiredFields: true,
          AllowMultipleValueFilterForTaxonomyFields: true
        }, {
          View: viewId,

        }, param);
      cnt++;
      console.log(res)

      if (res.Row && res.Row.length) {
        reslts.push(...res.Row);
      }
      if (res.NextHref) {
        await relad(res);
      }
    }
    async function aaa() {
      await relad();
      console.log(reslts.length);
      console.log(reslts.filter(i => i["Bool"] === "はい").length)
    }
    aaa();

  });
/**/

/* テストデータ投入 * /
async function add() {
  for (let i = 0; i < 20; i++) {
    const bat = sp.web.createBatch();
    for (let j = 0; j < 100;j++) {
      sp.web.lists.getByTitle("list1").items
      .inBatch(bat)
      .add({
        Title: "test2-"+ (("00000" + (j+(i*100))).slice(4)),
        ADate: new Date(),
        Num: j,
        IsTarget:true,
      });
    }
    await bat.execute().then(() => console.log(i));
  }
}
add();
/* */
/* 検索 */
